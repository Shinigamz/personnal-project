# BurgerQueen

BurgerQueen is a fake seamless in-store, online and mobile ordering experience inspired by Burger King UI.
BurgerQueen is a project that allowed me to deepen my knowledge of VueJS. 
Thanks to this project, I learned to design the Front End from scratch by drawing 
inspiration from a model on the web 
(http://caylatorgerson.com/wp-content/uploads/2017/03/bk-walmart_mm -1030x806.png) 
and to implement dynamic data with an external JSON.

/!\ All the images, structure, contents, etc... are for educational purposes only /!\

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
