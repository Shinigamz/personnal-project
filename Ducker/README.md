# Ducker

Ducker is a fake Twitter that allows to write Quake (Tweet) on a page. 
Ducker is a project that allowed me to create my first Fullstack application.
On Ducker, the user have to write his name and his message and send it. 
The application save his message and show it down below.
Thanks to this project, I learned to create my API (Express) linked to my MongoDB 
database (Monk).
I have also discovered how to prevent bad words, SQL injection and limit query rate.
This is my first clean fullstack application.


/!\ All the images, structure, contents, etc... are for educational purposes only /!\

> A Fullstack project

## Build Setup (You must have Mongo installed on your computer)

``` bash
# install dependencies on server
npm install

# serve with hot reload at localhost:5000
npm run dev

# then launch client
index.html

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
