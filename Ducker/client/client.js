console.log("Hello World");

const form = document.querySelector('form');
const loadingElement = document.querySelector('.loading');
const API_URL = "http://localhost:5000/quacks";
const quacksElement = document.querySelector('.quacks');

loadingElement.style.display='none';

listAllQuacks();

form.addEventListener('submit', (event)=> {
    event.preventDefault(); //By default we have content and name in url : this line allows js to hide them
    const formData = new FormData(form);
    const name = formData.get('name');
    const content = formData.get('content');

    const quack = {
        name,
        content
    };
    form.style.display = 'none'
    loadingElement.style.display='';
    
    fetch(API_URL, {
        method: 'POST',
        body: JSON.stringify(quack),
        headers: {
            'content-type': 'application/json'
        }
    }).then(response => response.json())
    .then(createdQuack => {
        form.reset();
        setTimeout(() => {
            form.style.display = '';
        }, 10000);
        loadingElement.style.display='none';
        listAllQuacks();
    });
});

function listAllQuacks(){
    quacksElement.innerHTML='';
    fetch(API_URL)
    .then(response => response.json())
    .then(quacks => {
        console.log(quacks);
        quacks.reverse();
        quacks.forEach(quack => {
            const div = document.createElement('div');
            const header = document.createElement('h3');
            header.textContent = quack.name;
            const contents = document.createElement('p');
            contents.textContent = quack.content;
            const date = document.createElement('small');
            date.textContent = new Date(quack.created);

            div.appendChild(header);
            div.appendChild(contents);
            div.appendChild(date);

            quacksElement.appendChild(div);
        });
    });
}