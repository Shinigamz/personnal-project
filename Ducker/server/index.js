//npm i --save-dev nodemon to refresh server after modification without kill it / restart server

const express = require('express');
const cors = require('cors');
const monk = require('monk');
const Filter = require('bad-words');
const rateLimit= require("express-rate-limit");


const app = express();

const db = monk(process.env.MONGO_URI || 'localhost/ducker');
const quacks = db.get('quacks');
const filter = new Filter();


app.use(cors());
app.use(express.json()); //JSON body parser


app.get("/", (request, response) => {
    response.json({
        message: "Quack quack !!"
    });
});

app.get("/quacks", (request, response) => {
    quacks
    .find()
    .then(quacks => {
        response.json(quacks);
    });
});

function isValidQuack(quack) {
    return quack.name && quack.name.toString().trim() !== '' &&  quack.content && quack.content.toString().trim() !== '';
}

// we put this here to affect only the request post
app.use(rateLimit({
    windowMs: 10 * 1000, // 30 seconds
    max: 1 // limit each IP to 100 request per windowMs
}));

app.post("/quacks", (request, response) => {
    if(isValidQuack(request.body)){
        //insert into db
        const quack = {
            name: filter.clean(request.body.name.toString()),
            content: filter.clean(request.body.content.toString()),
            created: new Date()
        };
        quacks.insert(quack).then(createdQuack => {
            response.json(createdQuack);
        });
    } else {
        response.status(422);
        response.json({
            message: 'Hey ! Name and Content are required!'
        });
    }
});

app.listen(5000, () => {
    console.log('Listening on http://localhost:5000')
})