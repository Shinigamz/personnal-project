import React, { useEffect, useState } from 'react';
import{
  MenuItem, 
  FormControl,
  Select,
  Card, 
  CardContent,
} from '@material-ui/core';
import './App.css';
import InfoBox from './InfoBox';
import Map from './Map';
import Table from './Table';
import LineGraph from './LineGraph';
import "leaflet/dist/leaflet.css";
import numeral from "numeral";
import { sortData } from "./util"
import { prettyPrintStat } from "./util"

function App() {
  const [countries, setCountries] = useState([]);
  // STATE = How to write a variable in REACT
  const [country, setCountry] = useState('worldwide');
  // https://disease.sh/v3/covid-19/countries
  const [countryInfo, setCountryInfo] = useState([]);
  const [tableData, setTableData] = useState([]);

  const[mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -40.4796});
  const[mapZoom, setMapZoom] = useState(3);
  const[mapCountries, setMapCountries] = useState([]);

  const[casesType, setCasesType] = useState("cases");

  //USEEFFECT = Runs a piece of code based on a given condition

  useEffect(() => {
    fetch("https://disease.sh/v3/covid-19/all")
    .then(response => response.json())
    .then(data => {
      setCountryInfo(data);
    });
  }, []);

  useEffect(() => {
    //This code will run once when the component loads and not again
    // async -> send a request, wait and do smthing with the infos
    const getCountriesData = async () => {
      await fetch("https://disease.sh/v3/covid-19/countries")
      .then((response) => response.json())
      .then((data) => {
        const countries = data.map((country) => (
          {
            name: country.country,
            value: country.countryInfo.iso2
          }));

        const sortedData = sortData(data);
        setTableData(sortedData);  
        setCountries(countries);
        setMapCountries(data);
      });
    };
    getCountriesData();
  }, []);

  const onCountryChange = async (event) => {
    const countryCode = event.target.value;
    setCountry(countryCode);
    // https://disease.sh/v3/covid-19/countries/all >>> Worldwide
    // https://disease.sh/v3/covid-19/countries/[COUNTRY_CODE] >>> If country is selected

    const url = 
      countryCode === 'worldwide' 
        ? "https://disease.sh/v3/covid-19/all"
        : `https://disease.sh/v3/covid-19/countries/${countryCode}`;

    await fetch(url)
    .then(response => response.json())
    .then((data) => {
      setCountry(countryCode);

      // All of the data from the country response
      setCountryInfo(data);
      setMapCenter([data.countryInfo.lat, data.countryInfo.long]);
      setMapZoom(4);
    });

  };

  return (
    <div className="app"> 
      <div className="app_left">
        {/* Header */}
        {/* Title + Select input dropdown field */}
        <div className="app_header">
          <h1>COVID-19 Tracker</h1>
          <FormControl className="app_dropdown"> {/* BEM Name */}
            <Select variant="outlined" value={country} onChange={onCountryChange} >
            <MenuItem value="worldwide">Worldwide</MenuItem>
            {
              countries.map(country => (
                <MenuItem value={country.value}>{country.name}</MenuItem>
              ))
            }
            </Select>
          </FormControl>
        </div>
        <div className="app_stats">
          {/* InfoBoxs */}
          <InfoBox onClick={(e) => setCasesType("cases")} title="Coronavirus cases" cases={prettyPrintStat(countryInfo.todayCases)} isRed active={casesType === "cases"} total={numeral(countryInfo.cases).format("0.0a")}/>
          {/* InfoBoxs */}
          <InfoBox onClick={(e) => setCasesType("recovered")} title="Recovered" active={casesType === "recovered"} cases={prettyPrintStat(countryInfo.todayRecovered)} total={numeral(countryInfo.recovered).format("0.0a")} />
          {/* InfoBoxs */}
          <InfoBox onClick={(e) => setCasesType("deaths")} title="Deaths" isRed active={casesType === "deaths"} cases={prettyPrintStat(countryInfo.todayDeaths)} total={numeral(countryInfo.deaths).format("0.0a")} />
          
        </div>

          {/* Map */}
          <Map 
            countries={mapCountries}
            casesType={casesType}
            center={mapCenter} 
            zoom={mapZoom}
          />
      </div> 
      <Card className="app_right">
        <CardContent>
        <h3>Live Cases by Country</h3>
        {/* Table */}
        <Table countries={tableData} />
        <h3 className="app_graphTitle">Worldwide new {casesType}</h3>
        {/* Graph */}
        <LineGraph className="app_graph" casesType={casesType} />
        </CardContent>
      </Card>
    </div>
  );
}
export default App;
